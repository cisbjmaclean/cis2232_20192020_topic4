/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.camperlookupproject;

import javax.swing.JOptionPane;

/**
 *
 * @author bjmaclean
 */
public class Controller {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        CamperSoap_Service camperService = new CamperSoap_Service();
        CamperSoap camper = camperService.getCamperSoapPort();

        String idEntered = JOptionPane.showInputDialog("Enter id to lookup");
        String camperFoundName = camper.getCamper(idEntered);
        System.out.println(camperFoundName);
        JOptionPane.showMessageDialog(null, "The camper's name is: "+camperFoundName);
        
    }

}
